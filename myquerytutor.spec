%define name myquerytutor
%define version 2.1.1
%define unmangled_version 2.1.1
%define release 1

Summary: Educational tool to teach SQL
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}-%{unmangled_version}.tar.gz
License: LGPL-3.0-or-later
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
Vendor: Steven Tucker <tuxta2@gmail.com>
Url: https://gitlab.com/tuxta/myquerytutor

BuildRequires: python3
BuildRequires: python3-qt5
BuildRequires: python3-qtwebengine-qt5
BuildRequires: python3-beautifulsoup4
BuildRequires: python3-requests
BuildRequires: python3-setuptools

Requires: python3
Requires: python3-qt5
Requires: python3-qtwebengine-qt5
Requires: python3-beautifulsoup4
Requires: python3-requests
Requires: python3-setuptools

%description
MyQueryTutor is an educational tool to teach SQL.
It contains a series of exercises with progress tracking.
MyQueryTutor provides tutorials, instant feedback and optionally
server sync for use in classrooms.

%prep
%setup -n %{name}-%{unmangled_version} -n %{name}-%{unmangled_version}

%build
python3 setup.py build

%install
python3 setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES

mkdir -p $RPM_BUILD_ROOT/usr/share/applications/

cat > $RPM_BUILD_ROOT/usr/share/applications/%{name}.desktop <<'EOF'
[Desktop Entry]
Name=myquerytutor
GenericName=MyQueryTutor
Comment=Learning tool for SQL
Exec=myquerytutor
Icon=%{python3_sitelib}/myquerytutor/mqt.ico
Categories=Development;IDE;
Type=Application
Encoding=Legacy-Mixed
Terminal=0
EOF

%clean
rm -rf $RPM_BUILD_ROOT

%files -f INSTALLED_FILES
%{_datadir}/applications/%{name}.desktop

%defattr(-,root,root)
